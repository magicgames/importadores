--- OBTENEMOS LOS DESTINOS POR AEROPUERTO

select distinct(t."DestinationCode") , t."TerminalCode" ,d."Name"
from hotelbeds_terminaltransferzone as t inner join hotelbeds_terminal as te
on t."TerminalCode"=te."TerminalCode"
inner join hotelbeds_destinationdescription as d on d."DestinationCode" = t."DestinationCode"
where d."LanguageCode"='CAS' and te."TerminalTypeCode"='A' order by t."TerminalCode" asc;


-- Obtenemos las Zonas con Destinos y Terminales

select distinct(t."DestinationCode") , t."TerminalCode" ,d."Name",z."ZoneName",z."ZoneCode"
from hotelbeds_terminaltransferzone as t inner join hotelbeds_terminal as te
on t."TerminalCode"=te."TerminalCode"
inner join hotelbeds_destinationdescription as d on d."DestinationCode" = t."DestinationCode"
inner join hotelbeds_zone z on d."DestinationCode"=z."DestinationCode"
where d."LanguageCode"='CAS' and te."TerminalTypeCode"='A' order by t."TerminalCode" asc;


-- Obtenemos los Hoteles

select h."DestinationCode",de."Name",z."ZoneName",h."ZoneCode",h."Name",h."HotelCode"
from hotelbeds_hotel h

inner join hotelbeds_destinationdescription as de on de."DestinationCode" = h."DestinationCode"
inner join hotelbeds_destination as d on d."DestinationCode" = de."DestinationCode"
inner join hotelbeds_zone z on d."DestinationCode"=z."DestinationCode"
inner join hotelbeds_terminaltransferzone as t on h."DestinationCode"=t."DestinationCode"
inner join hotelbeds_countrydescription as cd on d."CountryCode" = cd."CountryCode"
inner join hotelbeds_hoteldescription hd on h."HotelCode"=hd."HotelCode"
where de."LanguageCode"='CAS' and cd."LanguageCode"='CAS'and hd."LanguageCode"='CAS'
