import ftplib
import socket
import zipfile
import shutil
import datetime
import os
import sys
from os.path import join, abspath, dirname

BASE_DIR = dirname(dirname(dirname(__file__)))
PUBLIC_DIR = join(BASE_DIR, 'public')
PROVIDER_DATA_DIR = join(BASE_DIR, 'provider_data')

BASE_DIR = dirname(dirname(__file__))
host = 'ftp.hotelbeds.com'
user = 'TOClient'
password = 'HB2006'
path = 'ExportCSV/'
file_name = 'Extended.zip'
download_path ='/tmp'


def get_file():
    try:
        modifiedTime = os.path.getmtime(download_path)
        timeStamp = datetime.fromtimestamp(modifiedTime).strftime("%b-%d-%y-%H:%M:%S")
        shutil.move(download_path, download_path + "_" + timeStamp)  # Borramos el directorio
    except Exception as e:
        print e
        pass

    downloaded_file = False
    try:
        ftp = ftplib.FTP(host)
    except (socket.error, socket.gaierror), e:
        print 'ERROR: cannot reach "%s"' % host
        return downloaded_file
    try:
        ftp.login(user, password)
    except ftplib.error_perm:
        print 'ERROR: cannot login anonymously'
        ftp.quit()
        return downloaded_file

    try:
        ftp.cwd(path)
    except ftplib.error_perm:
        print 'ERROR: cannot CD to "%s"' % path
        ftp.quit()
        return downloaded_file

    try:
        if not os.path.exists(download_path):
            os.makedirs(download_path)
        file_path = os.path.join(download_path, file_name)
        ftp.retrbinary('RETR %s' % file_name, open(file_path, 'wb').write)
    except ftplib.error_perm:
        print 'ERROR: cannot read file "%s"' % file_name
        os.unlink(file_name)
        return downloaded_file
    else:
        ftp.quit()
        zip = zipfile.ZipFile(file_path)
        zip.extractall(path=download_path)
        downloaded_file = True
        return downloaded_file

if __name__ == "__main__":
    get_file()
