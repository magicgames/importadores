from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, Text, Numeric
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.orm import relationship
from connection import engine

Base = declarative_base()
session = scoped_session(sessionmaker())


class BaseEntity(object):
    __table_args__ = {
            'mysql_engine': 'InnoDB',
            'mysql_charset': 'utf8'
    }

class HotelBedsLanguage(Base,BaseEntity):
    __tablename__ = 'hotelbeds_language'
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    LanguageName = Column(String(15), nullable=False)

    def create_file(self):
        return "Languages.csv"

    def update_file(self):
        return 'LanguagesUpdates.csv'


class HotelBedsCategory(Base,BaseEntity):
    __tablename__ = 'hotelbeds_category'
    CategoryCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "Categories.csv"

    def update_file(self):
        return 'CategoriesUpdates.csv'


class HotelBedsCategoryDescription(Base,BaseEntity):
    __tablename__ = 'hotelbeds_categorydescription'
    CategoryCode = Column(String(20), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    CategoryName = Column(String(400), nullable=False)

    def create_file(self):
        return "CategoryDescriptions.csv"

    def update_file(self):
        return 'CategoryDescriptionsUpdates.csv'


class HotelBedsCountry(Base,BaseEntity):
    __tablename__ = 'hotelbeds_country'
    CountryCode = Column(String(20), primary_key=True, index=True)

    def create_file(self):
        return "Countries.csv"

    def update_file(self):
        return 'CountriesUpdates.csv'


class HotelBedsCountryDescription(Base,BaseEntity):
    __tablename__ = 'hotelbeds_countrydescription'
    CountryCode = Column(String(20), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(190), nullable=False)
    ShortName = Column(String(20), nullable=False)

    def create_file(self):
        return "CountryIDs.csv"

    def update_file(self):
        return 'CountryIDsUpdates.csv'


class HotelBedsDestination(Base,BaseEntity):
    __tablename__ = "hotelbeds_destination"
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)
    CountryCode = Column(String(20), nullable=True)

    def create_file(self):
        return "Destinations.csv"

    def update_file(self):
        return 'DestinationsUpdates.csv'


class HotelBedsDestinationDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_destinationdescription"
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(190), nullable=False)

    def create_file(self):
        return "DestinationIDs.csv"

    def update_file(self):
        return 'DestinationIDsUpdates.csv'


class HotelBedsChain(Base,BaseEntity):
    __tablename__ = 'hotelbeds_chain'
    ChainCode = Column(String(20), primary_key=True, index=True, nullable=False)
    ChainName = Column(String(400), nullable=False)

    def create_file(self):
        return "Chains.csv"

    def update_file(self):
        return 'ChainsUpdates.csv'


class HotelBedsZone(Base,BaseEntity):
    __tablename__ = "hotelbeds_zone"
    ZoneCode = Column(Integer, primary_key=True, index=True, nullable=False)
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)
    ZoneName = Column(String(400), nullable=False)

    def create_file(self):
        return "Zones.csv"

    def update_file(self):
        return 'ZonesUpdates.csv'


class HotelBedsHotelReviews(Base,BaseEntity):
    __tablename__ = 'hotelbeds_hotelreview'
    HotelCode = Column(Integer, primary_key=True, index=True)
    ReviewCode = Column(Integer, primary_key=True, index=True)
    LocationReview = Column(Text, nullable=True)
    NegativeReview = Column(Text, nullable=True)
    PositiveReview = Column(Text, nullable=True)
    Review = Column(Text, nullable=True)
    Date = Column(String(21), nullable=True)
    Value = Column(String(30), nullable=True)
    Name = Column(String(190), nullable=True)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "Reviews.csv"

    def update_file(self):
        return 'ReviewsUpdates.csv'


class HotelBedsHotelDescription(Base,BaseEntity):
    __tablename__ = 'hotelbeds_hoteldescription'

    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    HotelFacilities = Column(Text())
    HotelLocationDescription = Column(Text())
    HotelRoomDescription = Column(Text())
    HolelSportDescription = Column(Text())
    HotelMealsDescription = Column(Text())
    HotelPaymentMethods = Column(Text())
    HotelHowToGetThere = Column(String(45))
    HotelComments = Column(String(45))

    def create_file(self):
        return "HotelDescriptions.csv"

    def update_file(self):
        return 'HotelDescriptionsUpdates.csv'


class HotelBedsImageType(Base,BaseEntity):
    __tablename__ = 'hotelbeds_imagetype'
    ImageCode = Column(String(5), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "ImageTypes.csv"

    def update_file(self):
        return 'ImageTypesUpdates.csv'


class HotelBedsImageTypeDescription(Base,BaseEntity):
    __tablename__ = 'hotelbeds_imagetypedescription'
    ImageCode = Column(String(5), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(400), nullable=False)

    def create_file(self):
        return "ImageDescriptions.csv"

    def update_file(self):
        return 'ImageDescriptionsUpdates.csv'


class HotelBedsImage(Base,BaseEntity):
    __tablename__ = 'hotelbeds_image'

    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    ImageCode = Column(String(5), primary_key=True, index=True, nullable=False)
    Order = Column(Integer, primary_key=True, index=True)
    VisualizationOrder = Column(String(20), nullable=True)
    ImagePath = Column(String(190),nullable=False,primary_key=True)

    def create_file(self):
        return "HotelImages.csv"

    def update_file(self):
        return 'HotelImagesUpdates.csv'


class HotelBedsPhoneType(Base,BaseEntity):
    __tablename__ = 'hotelbeds_phonetype'
    PhoneType = Column(String(15), primary_key=True, index=True, nullable=False)
    Name = Column(String(190), nullable=False)

    def create_file(self):
        return "PhoneTypes.csv"

    def update_file(self):
        return 'PhoneTypesUpdates.csv'


class HotelBedsPhone(Base,BaseEntity):
    __tablename__ = 'hotelbeds_phone'

    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    PhoneType = Column(String(15), nullable=True, primary_key=True, index=True)
    Name = Column(String(400), nullable=True)

    def create_file(self):
        return "Phones.csv"

    def update_file(self):
        return 'PhonesUpdates.csv'


class HotelBedsZoneGroup(Base,BaseEntity):
    __tablename__ = "hotelbeds_zonegroup"
    InterfaceCode = Column(String(1), primary_key=True, index=True, nullable=False)
    GroupZoneCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "ZoneGroup.csv"

    def update_file(self):
        return 'ZoneGroupUpdates.csv'


class HotelBedsZoneGroupDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_zonegroupdescription"
    InterfaceCode = Column(String(1), primary_key=True, index=True, nullable=False)
    GroupZoneCode = Column(String(20), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Description = Column(String(400), nullable=False)

    def create_file(self):
        return "ZoneGroupDescriptions.csv"

    def update_file(self):
        return 'ZoneGroupDescriptionsUpdates.csv'


class HotelBedsZoneByGroup(Base,BaseEntity):
    __tablename__ = "hotelbeds_zonebygroup"
    InterfaceCode = Column(String(1), primary_key=True, index=True, nullable=False)
    GroupZoneCode = Column(String(20), primary_key=True, index=True, nullable=False)
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)
    ZoneCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "ZoneByGroup.csv"

    def update_file(self):
        return 'ZoneByGroupUpdates.csv'


class HotelBedsHotelIssue(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelissue"
    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    IssueOrder = Column(Integer, primary_key=True, index=True, nullable=False)
    DateFrom = Column(String(21), nullable=True)
    DateTo = Column(String(21), nullable=True)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Incident = Column(String(1900), nullable=False)

    def create_file(self):
        return "HotelIssues.csv"

    def update_file(self):
        return 'HotelIssuesUpdates.csv'


class HotelBedsHotelFacilityTipology(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelfacilitytypology"
    TypologyCode = Column(String(20), primary_key=True, index=True, nullable=False)
    TextFlag = Column(String(1), nullable=False)
    NumberFlag = Column(String(1), nullable=False)
    LogicFlag = Column(String(1), nullable=False)
    FeeFlag = Column(String(1), nullable=False)
    DistanceFlag = Column(String(1), nullable=False)
    WalkingDistanceFlag = Column(String(1), nullable=False)
    TransportDistanceFlag = Column(String(1), nullable=False)
    CarDistanceFlag = Column(String(1), nullable=False)
    AgeFroFlag = Column(String(1), nullable=False)
    AgeToFlag = Column(String(1), nullable=False)
    BeachAccessFlag = Column(String(1), nullable=False)
    BeachApartFlag = Column(String(1), nullable=False)
    DynamicFlag = Column(String(1), nullable=False)
    ConceptFlag = Column(String(1), nullable=False)

    def create_file(self):
        return "FacilityTypologies.csv"

    def update_file(self):
        return 'FacilityTypologiesUpdates.csv'


class HotelBedsHotelFacilityGroup(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelfacilitygroup"
    Group = Column(Integer, primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "FacilityGroups.csv"

    def update_file(self):
        return 'FacilityGroupsUpdates.csv'


class HotelBedsHotelFacilityGroupDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelfacilitygroupdescription"
    Group = Column(Integer, primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(190), nullable=False)

    def create_file(self):
        return "FacilityGroupsDescriptions.csv"

    def update_file(self):
        return 'FacilityGroupsDescriptionsUpdates.csv'


class HotelBedsHotelFacilityType(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelfacilitytype"
    Group = Column(Integer, primary_key=True, index=True, nullable=False)
    Code = Column(Integer, primary_key=True, index=True, nullable=False)
    TypologyCode = Column(String(20), nullable=False)
    BasicFlag = Column(String(1), nullable=False)

    def create_file(self):
        return "FacilityTypes.csv"

    def update_file(self):
        return 'FacilityTypesUpdates.csv'


class HotelBedsHotelFacility(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelfacility"
    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    Code = Column(Integer, primary_key=True, index=True, nullable=False)
    Group = Column(Integer, primary_key=True, index=True, nullable=False)
    Order = Column(Integer, primary_key=True, index=True, nullable=False)
    Number = Column(String(9), nullable=True)
    Logic = Column(String(1), nullable=True)
    Fee = Column(String(1), nullable=True)
    Distance = Column(String(9), nullable=True)
    WalkingDistance = Column(String(9), nullable=True)
    TransportDistance = Column(String(9), nullable=True)
    CarDistance = Column(String(9), nullable=True)
    AgeFrom = Column(String(20), nullable=True)
    AgeTo = Column(String(20), nullable=True)
    BeachAccess = Column(String(1), nullable=True)
    BeachApart = Column(String(1), nullable=True)
    Concept = Column(String(190), nullable=True)

    def create_file(self):
        return "Facilities.csv"

    def update_file(self):
        return 'FacilitiesUpdates.csv'


class HotelBedsHotelFacilityDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelfacilitydescription"
    Code = Column(Integer, primary_key=True, index=True, nullable=False)
    Group = Column(Integer, primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(190), nullable=False)

    def create_file(self):
        return "FacilityDescriptions.csv"

    def update_file(self):
        return 'FacilityDescriptionsUpdates.csv'


class HotelBedsBoardType(Base,BaseEntity):
    __tablename__ = "hotelbeds_boardtype"
    BoardType = Column(String(7), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "BoardTypes.csv"

    def update_file(self):
        return 'BoardTypesUpdates.csv'


class HotelBedsBoardTypeDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_boardescription"
    BoardType = Column(String(7), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    ShortName = Column(String(20), nullable=False)
    Name = Column(String(400), nullable=False)

    def create_file(self):
        return "BoardDescriptions.csv"

    def update_file(self):
        return 'BoardDescriptionsUpdates.csv'


class HotelBedsRegimByDestination(Base,BaseEntity):
    __tablename__ = "hotelbeds_regimbydestination"
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)
    BoardType = Column(String(7), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "RegimByDest.csv"

    def update_file(self):
        return 'RegimByDestUpdates.csv'


class HotelBedsTerminalType(Base,BaseEntity):
    __tablename__ = "hotelbeds_terminaltype"
    TerminalTypeCode = Column(String(1), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "TerminalTypes.csv"

    def update_file(self):
        return 'TerminalTypesUpdates.csv'


class HotelBedsTerminalDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_terminaltypedescription"
    TerminalTypeCode = Column(String(1), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(400), nullable=False)

    def create_file(self):
        return "TerminalTypesDescriptions.csv"

    def update_file(self):
        return 'TerminalTypesDescriptionsUpdates.csv'


class HotelBedsTerminal(Base,BaseEntity):
    __tablename__ = "hotelbeds_terminal"
    TerminalCode = Column(String(6), primary_key=True, index=True, nullable=False)
    TerminalName = Column(String(60), nullable=False)
    TerminalTypeCode = Column(String(1), primary_key=True, index=True, nullable=False)
    CountryCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "TerminalsNaf.csv"

    def update_file(self):
        return 'TerminalsNafUpdates.csv'


class HotelBedsHotelTerminal(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelterminal"
    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    TerminalCode = Column(String(6), primary_key=True, index=True, nullable=False)
    DistKm = Column(String(20), nullable=True)
    DistMin = Column(String(20), nullable=True)

    def create_file(self):
        return "HotelTerminals.csv"

    def update_file(self):
        return 'HotelTerminalsUpdates.csv'


class HotelBedsHotelContact(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelcontact"
    HotelCode = Column(Integer, primary_key=True, index=True, nullable=False)
    Address = Column(String(190), nullable=False)
    PostalCode = Column(String(20), nullable=True)
    City = Column(String(400), nullable=False)
    CountryCode = Column(String(20), nullable=False)
    Email = Column(String(190), nullable=True)
    Web = Column(String(190), nullable=True)

    def create_file(self):
        return "Contacts.csv"

    def update_file(self):
        return 'ContactsUpdates.csv'


class HotelBedsHotelRoomType(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelroomtype"
    RoomType = Column(String(20), primary_key=True, index=True, nullable=False)
    MinOccupation = Column(Integer)
    MaxOccupation = Column(Integer)
    StandardOccupation = Column(Integer)
    MaxAdult = Column(Integer)
    MaxChildren = Column(Integer)

    def create_file(self):
        return "RoomTypes.csv"

    def update_file(self):
        return 'RoomTypesUpdates.csv'


class HotelBedsHotelRoomTypeDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelroomtypedescription"
    RoomType = Column(String(20), primary_key=True, index=True, nullable=False)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    ShortName = Column(String(20), nullable=False)
    Name = Column(String(400), nullable=False)

    def create_file(self):
        return "RoomDescriptions.csv"

    def update_file(self):
        return 'RoomDescriptionsUpdates.csv'


class HotelBedsHotelDestination(Base,BaseEntity):
    __tablename__ = "hotelbeds_hoteldestination"
    CountryCode = Column(String(20), nullable=True)
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "HotelDestinations.csv"

    def update_file(self):
        return 'HotelDestinationsUpdates.csv'


class HotelBedsTicketsDestination(Base,BaseEntity):
    __tablename__ = "hotelbeds_ticketdestination"
    CountryCode = Column(String(20), nullable=True)
    DestinationCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "TicketDestinations.csv"

    def update_file(self):
        return 'TicketDestinationsUpdates.csv'


class HotelBedsTransferZone(Base,BaseEntity):
    __tablename__ = "hotelbeds_transferzone"
    ZoneTransfer = Column(String(19), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "TransferZone.csv"

    def update_file(self):
        return 'TransferZoneUpdates.csv'


class HotelBedsTransferZoneDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_transferzonedescription"
    ZoneTransfer = Column(String(19), primary_key=True, index=True)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Name = Column(String(400), nullable=False)

    def create_file(self):
        return "TransferZoneDescription.csv"

    def update_file(self):
        return 'TransferZoneDescriptionUpdates.csv'


class HotelBedsTerminalTransferZone(Base,BaseEntity):
    __tablename__ = "hotelbeds_terminaltransferzone"
    ZoneTransferTerminal = Column(String(19), primary_key=True, index=True)
    TerminalCode = Column(String(6), primary_key=True, index=True)
    ZoneTransfer = Column(String(19), primary_key=True, index=True)
    DestinationCode = Column(String(20), nullable=False)

    def create_file(self):
        return "TerminalTransferZone.csv"

    def update_file(self):
        return 'TerminalTransferZoneUpdates.csv'


class HotelBedsHotelRoomCharacteristic(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelroomcharacteristic"
    CharacteristicCode = Column(String(19), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "Characteristics.csv"

    def update_file(self):
        return 'CharacteristicsUpdates.csv'


class HotelBedsHotelRoomCharacteristicDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_hotelroomcharacteristicdescription"
    CharacteristicCode = Column(String(19), primary_key=True, index=True)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Description = Column(String(400), nullable=False)

    def create_file(self):
        return "CharacteristicDescriptions.csv"

    def update_file(self):
        return 'CharacteristicDescriptionsUpdates.csv'


class HotelBedsHotelTransferZone(Base,BaseEntity):
    __tablename__ = "hotelbeds_hoteltransferzone"
    ZoneTransfer = Column(String(19), primary_key=True, index=True)
    HotelCode = Column(String(20), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "HotelTransferZone.csv"

    def update_file(self):
        return 'HotelTransferZoneUpdates.csv'


class HotelBedsTransferZonePostalCode(Base,BaseEntity):
    __tablename__ = "hotelbeds_transferpostalcode"
    ZoneTransfer = Column(String(19), primary_key=True, index=True)
    PostalCode = Column(String(60), primary_key=True, index=True)

    def create_file(self):
        return "TransferZonePostalCode.csv"

    def update_file(self):
        return 'TransferZonePostalCodeUpdates.csv'


class HotelBedsTicketClassification(Base,BaseEntity):
    __tablename__ = "hotelbeds_ticketclassification"
    ClassificationCode = Column(String(5), primary_key=True, index=True, nullable=False)

    def create_file(self):
        return "TicketsClassification.csv"

    def update_file(self):
        return 'TicketsClassificationUpdates.csv'


class HotelBedsTicketClassificationDescription(Base,BaseEntity):
    __tablename__ = "hotelbeds_ticketclassificationdescription"
    ClassificationCode = Column(String(5), primary_key=True, index=True)
    LanguageCode = Column(String(20), primary_key=True, index=True, nullable=False)
    Description = Column(String(400), nullable=False)

    def create_file(self):
        return "TicketsClassificationDescription.csv"

    def update_file(self):
        return 'TicketsClassificationDescriptionUpdates.csv'


class HotelBedsHotel(Base,BaseEntity):
    __tablename__ = 'hotelbeds_hotel'
    HotelCode = Column(Integer, primary_key=True, index=True)
    Name = Column(String(70), nullable=False, )
    CategoryCode = Column(String(20), index=True)
    DestinationCode = Column(String(20), index=True)
    ZoneCode = Column(Integer, index=True)
    ChainCode = Column(String(20), nullable=True)
    License = Column(String(400))
    Latitude = Column(String(45))
    Longitude = Column(String(45))
    Images = relationship(HotelBedsImage, backref='images',
                          # POINT-2
                          primaryjoin="HotelBedsHotel.HotelCode==HotelBedsImage.HotelCode",
                          foreign_keys=[HotelBedsImage.__table__.c.HotelCode],

                          )

    def create_file(self):
        return "Hotels.csv"

    def update_file(self):
        return 'HotelsUpdates.csv'


def init_db():
    session.remove()
    session.configure(bind=engine, autoflush=False, expire_on_commit=False)
    Base.metadata.drop_all(engine, checkfirst=True)
    Base.metadata.create_all(engine)
    return engine


if __name__ == "__main__":
    init_db()
