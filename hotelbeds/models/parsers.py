

from slugify import slugify
# PARSEADOR DOCUMENTOS
def parse_hotel(result):
    for res in result:
        x = {
            '_id': slugify(res['hotelName']),
            'hotelCode': {
                'hotelBeds': res['hotelCode']
            },
            'hotelName': res['hotelName'],
            'hotelDescription': res['hotelDescription'],
            'numHotel': 1,
            'destinationName': res['destinationName'],
            'destinationCode': res['destinationCode'],
            'zoneCode': res['zoneCode'],
            'zoneName': res['zoneName'],
            'terminalCode': res['terminal'],
            'image': res['image'],
            'country': res['country'],
            'contact': res['contact'],
            'services': res['services'],
            'review': res['review']
        }

        yield x


def parse_destination(result):
    for res in result:
        yield {
            '_id': res[0],
            'destination_code': res[0],
            'num_hotel': res[3],
            'destination_name': res[2],
            'terminal_code': res[1],
            'country': res[4],
        }


def parse_zone(result):
    for res in result:
        x = {
            '_id': str(res[0]) + str(res[4]),
            'destination_code': res[0],
            'destination_name': res[2],
            'zone_code': res[4],
            'zone_name': res[3],
            'num_hotel': res[6],
            'terminal_code': res[1],
            'country': res[5],
        }

        yield x


