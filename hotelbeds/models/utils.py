def destination_get_destination(s, language):
    q = s.execute('select distinct(t.DestinationCode) , t.TerminalCode ,de.Name,b.num_hotels,cd.Name '
                  'from hotelbeds_terminaltransferzone as t inner join hotelbeds_terminal as te '
                  'on t.TerminalCode=te.TerminalCode '
                  'left join hotelbeds_destinationdescription as de on de.DestinationCode = t.DestinationCode '
                  'inner join hotelbeds_destination as d on d.DestinationCode = t.DestinationCode  '
                  'inner join hotelbeds_countrydescription as cd on d.CountryCode = cd.CountryCode '
                  'inner join ( select count(*) as num_hotels,DestinationCode from hotelbeds_hotel h '
                  'group by DestinationCode) as b on d.DestinationCode = b.DestinationCode '
                  'where de.LanguageCode=:lang and cd.LanguageCode=:lang and '
                  'te.TerminalTypeCode=:t_type  order by t.TerminalCode asc;',
                  {'lang': language, 't_type': 'A'})
    return q


def zone_get_zone(s, language):
    q = s.execute('select distinct(t.DestinationCode) , t.TerminalCode ,de.Name,z.ZoneName,z.ZoneCode,'
                  'cd.Name,b.num_hotels from hotelbeds_terminaltransferzone as t '
                  'inner join hotelbeds_terminal as te on t.TerminalCode=te.TerminalCode '
                  'inner join hotelbeds_destination as d on d.DestinationCode = t.DestinationCode '
                  'inner join hotelbeds_countrydescription as cd on d.CountryCode = cd.CountryCode '
                  'left join hotelbeds_destinationdescription as de on de.DestinationCode = t.DestinationCode '
                  'inner join hotelbeds_zone z on d.DestinationCode=z.DestinationCode '
                  'inner join ( select count(*) as num_hotels,DestinationCode,ZoneCode from hotelbeds_hotel h '
                  'group by DestinationCode,ZoneCode) as b on d.DestinationCode = b.DestinationCode '
                  'and z.ZoneCode=b.ZoneCode '
                  'where de.LanguageCode=:lang and  cd.LanguageCode=:lang '
                  'and te.TerminalTypeCode=:t_type', {'lang': language, 't_type': 'A'})
    return q


# Funciones auxiliares HOTEL

def get_terminal(s, hotel_code, destination_code):
    terminal = ''
    qt = s.execute('select t.TerminalCode from hotelbeds_terminal t '
                   'inner join hotelbeds_hotelterminal tz  on t.TerminalCode=tz.TerminalCode '
                   'inner join hotelbeds_hotel h on tz.HotelCode=h.HotelCode '
                   'where t.TerminalTypeCode=:t_type  and h.HotelCode=:h_code '
                   'order by h.HotelCode asc, tz.DistKm asc limit 1',
                   {'t_type': 'A', 'h_code': hotel_code})
    if qt.rowcount > 0:
        for t in qt:
            terminal = t[0]
    else:
        qt = s.execute('select t.TerminalCode '
                       'from hotelbeds_terminaltransferzone as t inner join hotelbeds_terminal as te '
                       'on t.TerminalCode=te.TerminalCode where  te.TerminalTypeCode=:t_type '
                       'and t.DestinationCode=:dest_code limit 1',
                       {'t_type': 'A', 'dest_code': destination_code}
                       )
        if qt.rowcount > 0:
            for t in qt:
                terminal = t[0]
        else:
            terminal = destination_code
    return terminal


def get_images(s, hotel_code):
    image_result = []
    qt = s.execute('select ImageCode,"Order",ImagePath from hotelbeds_image where HotelCode=:h_code',
                   {'h_code': int(hotel_code)})

    for i in qt:
        x = {

            'url': i[2],
            'code': i[0],
            'order': i[1],
        }
        image_result.append(x)
    return image_result


def get_description(s, hotel_code, language):
    description = ''
    qt = s.execute(
        'select  hd.HotelFacilities  from  hotelbeds_hoteldescription hd where hd.HotelCode=:h_code '
        'and hd.LanguageCode=:lang ',
        {'lang': language, 'h_code': int(hotel_code)})
    if qt.rowcount > 0:
        for t in qt:
            description = t[0]
    return description


def get_destination_name(s, destination_code, language):
    destination_name = ''
    qt = s.execute(
        'select de.Name from  hotelbeds_destinationdescription as de where de.DestinationCode=:h_dest '
        'and de.LanguageCode=:lang ',
        {'lang': language, 'h_dest': destination_code})
    if qt.rowcount > 0:
        for t in qt:
            destination_name = t[0]
    return destination_name


def get_contact(s, hotel_code, language):
    contact = []
    qt = s.execute(
        'SELECT Address, PostalCode, City, Email, Web,d.Name FROM hotelbeds_hotelcontact h '
        'inner join hotelbeds_countrydescription d on h.CountryCode=d.CountryCode '
        'where HotelCode=:h_code and d.LanguageCode=:lang',
        {'h_code': int(hotel_code), 'lang': language})

    if qt.rowcount > 0:
        for t in qt:
            x = {
                'address': t[0],
                'postalCode': t[1],
                'city': t[2],
                'email': t[3],
                'web': t[4],
                'country': t[5]

            }
            contact.append(x)
    return contact


def get_facilities(s, hotel_code):
    facilites = {}
    qt = s.execute('SELECT Code, "Group", "Order", Number, '
                   'Logic, Fee, Distance, WalkingDistance, TransportDistance, CarDistance, '
                   'AgeFrom, AgeTo, BeachAccess, BeachApart, Concept '
                   ' FROM hotelbeds_hotelfacility where HotelCode=:h_code', {'h_code': int(hotel_code)})

    if qt.rowcount > 0:
        free = True
        is_boolean = False
        for t in qt:
            code = t[0]
            group = t[1]
            order = t[2]
            number = t[3]
            logic = t[4]
            fee = t[5]
            distance = t[6]
            walking_distance = t[7]
            transport_distance = t[8]
            car_distance = t[9]
            age_from = t[10]
            age_to = t[11]
            beach_access = t[12]
            beach_apart = t[13]
            concept = t[14]

            if logic == 'S':
                is_boolean = True
                if fee == 'S':
                    free = False
                x = {
                    'isBoolean': is_boolean,
                    'free': free
                }
            else:

                x = {
                    'isBoolean': is_boolean,
                    'values': {
                        'number': number,
                        'distance': distance,
                        'walkingDistance': walking_distance,
                        'transportDistance': transport_distance,
                        'carDistance': car_distance,
                        'ageFrom': age_from,
                        'ageTo': age_to,
                        'beachAccess': beach_access,
                        'beachApart': beach_apart,
                        'concept': concept,

                    }

                }
            if group in facilites:
                if code not in facilites[group]:
                    facilites[group][code] = {}
                facilites[group][code].update(x)
            else:
                facilites[group] = {}
                facilites[group][code] = {}
                facilites[group][code].update(x)
    return facilites


def get_review(s, hotel_code):
    review = {}
    qt = s.execute(
        'select round(coalesce(sum(Value)/count(HotelCode),0),2) as media , count(HotelCode) as num_reviewsfrom  '
        'from hotelbeds_hotelreview  where HotelCode=:h_code', {'h_code': int(hotel_code)})

    for t in qt:
        num_reviews = t[1]
        average = t[0]
        review = {'numReview': num_reviews, 'average': average}

    return review


def get_hotel_result(s, language):
    hotel_result = []

    q = s.execute(
        'select h.HotelCode,h.Name,h.DestinationCode,h.ZoneCode,z.ZoneName,cd.Name from hotelbeds_hotel h '
        'inner join hotelbeds_zone z on h.DestinationCode=z.DestinationCode and h.ZoneCode=z.ZoneCode '
        'inner join hotelbeds_destination as d on d.DestinationCode = h.DestinationCode '
        'inner join hotelbeds_countrydescription as cd on d.CountryCode = cd.CountryCode '
        'where cd.LanguageCode=:lang',
        {'lang': language, 't_type': 'A'})

    for r in q:
        hotel_code = r[0]
        destination_code = r[2]
        hotel_name = r[1]
        zone_code = r[3]
        zone_name = r[4]
        country = r[5]
        contact = get_contact(s, hotel_code, language)
        description = get_description(s, hotel_code, language)
        destination_name = get_destination_name(s, destination_code, language)
        terminal = get_terminal(s, hotel_code, destination_code)
        image_result = get_images(s, hotel_code)
        facilites = get_facilities(s, hotel_code)
        review = get_review(s, hotel_code)

        x = {

            'hotelCode': hotel_code,
            'hotelName': hotel_name,
            'destinationCode': destination_code,
            'destinationName': destination_name,
            'zoneCode': zone_code,
            'zoneName': zone_name,
            'hotelDescription': description,
            'country': country,
            'terminal': terminal,
            'image': image_result,
            'contact': contact,
            'services': facilites,
            'review': review,
        }

        hotel_result.append(x)
    return hotel_result
