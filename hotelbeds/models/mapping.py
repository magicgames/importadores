import string
# CREACION DE INDICES


def create_preferent_index(client, index, language):
    real_index = index + '_' + string.lower(language)
    client.indices.create(index=real_index,
                          body={
                              "analysis": {
                                  "filter": {
                                      "nGram_filter": {
                                          "type": "edgeNGram", "min_gram": 2, "max_gram": 20,
                                          "token_chars": ["letter", "digit", "punctuation", "symbol"]
                                      }
                                  },
                                  "analyzer": {
                                      "nGram_analyzer": {
                                          "type": "custom",
                                          "tokenizer": "whitespace",
                                          "filter": ["lowercase", "asciifolding", "nGram_filter"]
                                      },
                                      "whitespace_analyzer": {"type": "custom", "tokenizer": "whitespace",
                                                              "filter": ["lowercase", "asciifolding"]
                                                              }
                                  }
                              }
                          },  # ignore already existing index
                          ignore=400
                          )

    pass


def create_destination_mapping_index(client, index, language):
    real_index = index + '_' + string.lower(language)
    client.indices.put_mapping(
        index=real_index,
        doc_type='destination',
        body={
            "destination": {

                "_all": {
                    "analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                "properties": {

                    "destination_code": {
                        "type": "string",
                        "include_in_all": "false"
                    },
                    "num_hotel": {
                        "type": "integer",
                        "include_in_all": "false"
                    },
                    "terminal_code": {

                        "type": "string",
                        "include_in_all": "false"
                    },
                    "destination_name": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "boost": 2,
                        "include_in_all": "true"
                    },

                    "country": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "include_in_all": "true",
                        "search_analyzer": "whitespace_analyzer"

                    }
                }

            }
        }, ignore=409

    )

    pass


def create_zone_mapping_index(client, index, language):
    real_index = index + '_' + string.lower(language)
    client.indices.put_mapping(
        index=real_index,
        doc_type='zone',
        body={
            "zone": {

                "_all": {
                    "analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                "properties": {
                    "destination_code": {
                        "type": "string",
                        "include_in_all": "false"
                    },
                    "destination_name": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "boost": 2,
                        "include_in_all": "true"
                    },
                    "zone_code": {
                        "type": "string",
                        "include_in_all": "false"
                    },
                    "zone_name": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "boost": 15,
                        "include_in_all": "true"
                    },

                    "terminal_code": {

                        "type": "string",
                        "include_in_all": "false"
                    },

                    "num_hotel": {
                        "type": "integer",
                        "include_in_all": "false"
                    },

                    "country": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "include_in_all": "true",

                    }
                }

            }
        }, ignore=409

    )

    pass


def create_hotel_mapping_index(client, index, language):
    real_index = index + '_' + string.lower(language)
    client.indices.put_mapping(
        index=real_index,
        doc_type='hotel',
        body={
            "hotel": {

                "_all": {
                    "analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                "properties": {

                    "review": {
                        "properties": {
                            "numReview": {
                                "type": "integer",
                                "null_value": 0
                            },
                            "average": {
                                "type": "float",
                                "null_value": 0
                            },

                        }
                    },

                    "hotelCode": {
                        "properties": {
                            "hotelBeds": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"

                            }

                        },

                    },
                    "facilities": {
                        "properties": {
                            "code": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"

                            },
                            "free": {
                                "type": "boolean"

                            }
                        }
                    },
                    "image": {

                        "properties": {
                            "url": {"type": "string",
                                    "index": "not_analyzed",
                                    "include_in_all": "false"},
                            "code": {"type": "string",
                                     "index": "not_analyzed",
                                     "include_in_all": "false"},
                            "order": {"type": "string",
                                      "index": "not_analyzed",
                                      "include_in_all": "false"},
                        }
                    },
                    "contact": {

                        "properties": {
                            "address": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"
                            },
                            "postalCode": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"
                            },
                            "city": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"
                            },
                            "email": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"
                            },
                            "web": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"
                            },
                            "country": {
                                "type": "string",
                                "index": "not_analyzed",
                                "include_in_all": "false"
                            },
                        }
                    },
                    "name": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "include_in_all": "true",
                        "boost": 100
                    },
                    "hotelDescription": {
                        "type": "string",
                        "index": "not_analyzed",
                        "include_in_all": "false"

                    },
                    "destinationCode": {
                        "type": "string",
                        "index": "not_analyzed",
                        "include_in_all": "false"
                    },

                    "destinationName": {
                        "type": "string",
                        "index": "not_analyzed",
                        "include_in_all": "false"
                    },
                    "zoneCode": {
                        "type": "string",
                        "index": "not_analyzed",
                        "include_in_all": "false"
                    },
                    "zoneName": {
                        "type": "string",
                        "index": "not_analyzed",
                        "include_in_all": "false"
                    },

                    "terminalCode": {

                        "type": "string",
                        "index": "not_analyzed",
                        "include_in_all": "false"
                    },

                    "numHotel": {
                        "type": "integer",
                        "include_in_all": "false"
                    },

                    "country": {
                        "type": "string",
                        "analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "include_in_all": "false"

                    }
                }

            }
        }, ignore=409

    )

    pass

