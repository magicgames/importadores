import string
from models import *
from utils import *
from mapping import *
from parsers import *
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk, streaming_bulk
from connection import engine

# GENERACION DE DOCUMENTOS

def generate_destination_doc(s, client, index, language):
    real_index = index + '_' + string.lower(language)
    destination = destination_get_destination(s, language)

    for ok, result in streaming_bulk(
            client,
            parse_destination(destination),
            index=real_index,
            doc_type='destination',
            chunk_size=50  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/zone/%s' % (real_index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
            print(doc_id)


def generate_zone_doc(s, client, index, language):
    real_index = index + '_' + string.lower(language)
    zone = zone_get_zone(s, language)
    for ok, result in streaming_bulk(
            client,
            parse_zone(zone),
            index=real_index,
            doc_type='zone',
            chunk_size=50  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/destination/%s' % (real_index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
            print(doc_id)

    pass


def generate_hotel_doc(s, client, index, language):
    real_index = index + '_' + string.lower(language)
    hotel_result =get_hotel_result(s,language)
    for ok, result in streaming_bulk(
            client,
            parse_hotel(hotel_result),
            index=real_index,
            doc_type='hotel',
            chunk_size=50  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/hotel/%s' % (real_index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
            print(doc_id)

    pass

lang_arr = ['CAS', 'ENG']
index_name = 'preferent_booking'
# create a configured "Session" class
Session = sessionmaker(bind=engine)

# create a Session
session = Session()
es = Elasticsearch()

for lang in lang_arr:
    create_preferent_index(es, index_name, lang)
    create_destination_mapping_index(es, index_name, lang)
    create_zone_mapping_index(es, index_name, lang)
    create_hotel_mapping_index(es, index_name, lang)
    generate_destination_doc(session, es, index_name, lang)
    generate_zone_doc(session, es, index_name, lang)
    generate_hotel_doc(session, es, index_name, lang)
"""
for r in q:
    print r
"""
