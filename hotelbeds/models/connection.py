from sqlalchemy import create_engine
from sqlalchemy.pool import Pool
import logging
from sqlalchemy import event

logger = logging.getLogger(__name__)     

@event.listens_for(Pool, "connect")
def set_unicode(dbapi_conn, conn_record):
    cursor = dbapi_conn.cursor()
    try:
        cursor.execute("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'")
    except Exception as e:
        logger.debug(e)


engine = create_engine('mysql+mysqldb://preferentbooking:pepepotamo@localhost/preferentbooking_import_db?charset=utf8&local_infile=1')
