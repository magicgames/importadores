import models
import string
from os.path import join, dirname
import sys
import inspect
import os
import time

from os.path import join, abspath, dirname
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, Text, Numeric
from sqlalchemy.orm import scoped_session, sessionmaker
from connection import engine


Base = declarative_base()
session = scoped_session(sessionmaker())
BASE_DIR = dirname(dirname(abspath(__file__)))
PROVIDER_DIR = join(BASE_DIR, 'utils')
PROVIDER_DIR ='/tmp'
EXT = '/tmp/Extended'
Base = declarative_base()
session = scoped_session(sessionmaker())

session.configure(bind=engine, autoflush=False, expire_on_commit=False)


def importer(classname):
    start = time.time()
    try:
        con = engine.connect()
        a = classname()
        file_name = a.create_file()
        file_path = join(EXT, file_name)
        sql = "LOAD DATA LOCAL INFILE \"%s\" INTO TABLE %s CHARACTER SET UTF8 COLUMNS " \
              " TERMINATED BY '|' LINES " \
              "TERMINATED BY '\n';" %(file_path,a.__tablename__)
        trans = con.begin()
        con.execute(sql)
        trans.commit()
    except BaseException as e:
        trans.rollback()
        print e


data = models.HotelBedsLanguage()

# print data.__tablename__

# cursor.copy_from(file_name, self.model._meta.db_table, sep="|", null='', columns=columns)

if __name__ == "__main__":
    start = time.time()
    for name, data in inspect.getmembers(models, inspect.isclass):
        if string.find(name, 'HotelBeds') > -1:
            print '%s :' % name, repr(data)
            try:
                importer(data)
            except BaseException as e:
                print e

    print "Total", time.time() - start, "seconds."
